package practicasIf;

import java.util.Scanner;

public class Punto13 {

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
        	
        	System.out.println ("Ingrese el n�mero del mes");
        	int mes= sc.nextInt();		
			while(mes < 1 || mes > 12) {
				System.out.println("El n�mero ingresado no se encuentra dentro del rango predefinido, por favor ingresar otro n�mero" );
				System.out.println("Ingrese el n�mero de un mes ");
				 mes= sc.nextInt();
			}
				 switch(mes) {
				 case 1 :
					 System.out.println ("El mes ingresado es Enero y tiene " + 31 +  " d�as");
					 break;
				 case 2 :
					 System.out.println ("El mes ingresado es Febrero y tiene " + 28 +  " d�as");
					 break;
				 case 3 :
					 System.out.println ("El mes ingresado es Marzo y tiene " + 31 +  " d�as");
					 break;
				 case 4 :
					 System.out.println ("El mes ingresado es Abril y tiene " + 30 +  " d�as");
					 break;
				 case 5 :
					 System.out.println ("El mes ingresado es Mayo y tiene " + 31 +  " d�as");
					 break;
				 case 6 :
					 System.out.println ("El mes ingresado es Junio y tiene " + 30 +  " d�as");
					 break;
				 case 7 :
					 System.out.println ("El mes ingresado es Julio y tiene " + 31 +  " d�as");
					 break;
				 case 8 :
					 System.out.println ("El mes ingresado es Agosto y tiene " + 31 +  " d�as");
					 break;
				 case 9 :
					 System.out.println ("El mes ingresado es Septiembre y tiene " + 30 +  " d�as");
					 break;
				 case 10 :
					 System.out.println ("El mes ingresado es Octubre y tiene " + 31 +  " d�as");
					 break;
				 case 11 :
					 System.out.println ("El mes ingresado es Noviembre y tiene " + 30 +  " d�as");
					 break;
				 case 12 :
					 System.out.println ("El mes ingresado es Diciembre y tiene " + 31 +  " d�as");
					 break;
				 }
        
        }
    }
}