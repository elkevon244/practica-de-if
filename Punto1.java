package practicasIf;
import java.util.Scanner;

public class Punto1 {

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Ingrese la 1ra nota del alumno");
            int a= sc.nextInt();
            System.out.println("Ingrese la 2da nota del alumno");
            int b= sc.nextInt();
            System.out.println("Ingrese la 3ra nota del alumno");
            int c= sc.nextInt();

            float x=a+b+c;
            float z = x/3; 
            if(z<7){
            System.out.println("El promedio del alumno es de " + z + " por ende esta desaprobado");
            }
            else {
                System.out.println("El promedio del alumno es de " + z + " por ende esta aprobado");
            }
        }
}
}