package practicasIf;
import java.util.Scanner;

public class Punto3 {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {

			System.out.println("Ingrese el n�mero de un mes  ");
			int mes= sc.nextInt();		
			while(mes < 1 || mes > 12) {
				System.out.println("El n�mero seleccionado no se encuentra dentro del rango predefinido, por favor ingrese otro n�mero" );
				System.out.println("Ingrese el n�mero de un mes ");
				 mes= sc.nextInt();
			}
			
			if(mes == 1) {
				System.out.println("El mes ingresado es Enero y tiene " + 31 + " dias");
			}
			if(mes == 2) {
				System.out.println("El mes ingresado es Febrero y tiene " + 28 + " dias");
			}
			if(mes == 3) {
				System.out.println("El mes ingresado es Marzo y tiene " + 31 + " dias");
			}
			if(mes == 4) {
				System.out.println("El mes ingresado es Abril y tiene " + 30 + " dias");
			}
			if(mes == 5) {
				System.out.println("El mes ingresado es Mayo y tiene " + 31 + " dias");
			}
			if(mes == 6) {
				System.out.println("El mes ingresado es Junio y tiene " + 30 + " dias");
			}
			if(mes == 7) {
				System.out.println("El mes ingresado es Julio y tiene " + 31 + " dias");
			}
			if(mes == 8) {
				System.out.println("El mes ingresado es Agosto y tiene " + 31 + " dias");
			}
			if(mes == 9) {
				System.out.println("El mes ingresado es Septiembre tiene " + 30 + " dias");
			}
			if(mes == 10) {
				System.out.println("El mes ingresado es Octubre y tiene " + 31 + " dias");
			}
			if(mes == 11) {
				System.out.println("El mes ingresado es Noviembre y tiene " + 30 + " dias");
			}
			if(mes == 12) {
				System.out.println("El mes ingresado es Diciembre y tiene " + 31 + " dias");
			}
			
			
	}

}
}
