package practicasIf;

import java.util.Scanner;

public class Punto11 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char x; 
        char a = 'a';
        char e = 'e';
        char i = 'i';
        char o = 'o';
        char u = 'u';
      
        System.out.println("Ingresa un carcter: ");
        x = sc.next().charAt(0);
        
        if(x== a|| x==e|| x== i|| x== o|| x== u) {
        	
        	System.out.println("El carcter ingresado (" + x + ") es una vocal ");
        }
        else {
        	System.out.println ("El carcter ingresado no esta dentro de las vocales");
        }
}
}